import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {ConfigService} from './config.service';

@Injectable({
	providedIn: 'root'
})
export class ConfigGuard implements CanActivate {
	constructor(
		private configService: ConfigService,
		private router: Router) {
	}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		if (!this.configService.xanoApiUrl.value) {
			this.router.navigate(['']);
			return false;
		} else {
			return true;
		}
	}

}
