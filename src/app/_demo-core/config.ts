import { DemoConfig } from './_demo-core'

export const config: DemoConfig = {
	title: 'Outseta Login',
	summary: '',
	marketplace_type: 'extension',
	editLink: 'https://gitlab.com/xano-marketplace/outseta/',
	components: [
		{
			name: 'Home',
			description: 'This component contains a card where users have two ways they can authenticate into your application. ' +
				'The first is a standard Xano signup login process, the second is with Outseta, where users can signup and login using Outseta.  ',
		},
		{
			name: 'Guarded Route',
			description: '' +
				'This component is only accessible if authenticated and will get /auth/me by passing back the auth token and show name' +
				' of user to demonstrate that everything is working properly in the auth process. The route further calls /outseta/profile if an account is linked. ' +
				'And you can link and unlink your Outseta account here.',
		},

	],
	instructions: [
		'Install the extension in your Xano Workspace',
		'For this extension you will need a <a href="https://www.outseta.com/" target="_blank">Outseta Account</a>',
		'Go to the newly added Outseta API Group and copy your API BASE URL',
		'Finally, in this demo, paste this API BASE URL in as "Your Xano API URL"',
	],
	requiredApiPaths: [
		'/auth/login',
		'/auth/signup',
		'/auth/outseta',
		'/auth/me',
		'/outseta/link_account',
		'/outseta/unlink_account',
		'/outseta/profile'
	]
};

