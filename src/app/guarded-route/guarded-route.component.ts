import { Component, OnInit } from '@angular/core'
import { ConfigService } from 'src/app/_demo-core/config.service'
import { ControlSchema } from 'src/app/_demo-core/form-generator/form-generator.component'
import { FormGroup } from '@angular/forms'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-guarded-route',
  templateUrl: './guarded-route.component.html',
  styleUrls: ['./guarded-route.component.scss'],
})
export class GuardedRouteComponent implements OnInit {
  constructor(
    private configService: ConfigService,
    private snackBar: MatSnackBar,
  ) {
  }

  public form: FormGroup
  public formSchema: ControlSchema[] = [
    { name: 'outseta_username', type: 'email', label: 'Outseta Username', validators: ['required'], icon: 'mail' },
    { name: 'outseta_password', type: 'password', label: 'Outseta Password', icon: 'lock', validators: ['required'] },
  ]
  public user;
  public ready: boolean = false;
  public accountLinked: boolean = false
  public outsetaProfile;

  ngOnInit(): void {
    this.configService.xanoAPI('/auth/me', 'get', null, {}, true)
      .subscribe(res => {
        this.user = res
        this.accountLinked = !!res?.outseta?.accountUid
        this.ready = true;

        if (this.accountLinked) {
          this.configService.xanoAPI('/outseta/profile', 'get', null, {}, true)
            .subscribe(
                res => this.outsetaProfile = res,
                error => this.configService.showErrorSnack(error)
            )
        }
      }, error => {
        this.ready = true;
        this.configService.showErrorSnack(error)
      })
  }

  public linkAccount(): void {
    this.configService.xanoAPI('/outseta/link_account', 'post', this.form.getRawValue(), {}, true)
      .subscribe(
        res => {
          this.snackBar.open(res.message, 'Success')
          this.accountLinked = !!res;
          if (this.accountLinked) {
            this.configService.xanoAPI('/outseta/profile', 'get', null, {}, true)
              .subscribe(res =>
                this.outsetaProfile = res,
                  error => this.configService.showErrorSnack(error)
              )
          }
        },
        error => this.configService.showErrorSnack(error),
      )
  }

  public unlinkAccount(): void {
    this.configService.xanoAPI('/outseta/unlink_account', 'post', {}, {}, true)
      .subscribe(
        res => {
          this.snackBar.open(res?.message, 'Success');
          this.outsetaProfile = undefined
          this.accountLinked = false;
        },
        error => this.configService.showErrorSnack(error)
      )
  }

}
