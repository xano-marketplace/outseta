# Outseta Auth

This demo illustrates the login process for Outseta. The extension provides your users with the ability to authenticate
into your app using their Outseta credentials as well as allowing your registered users to link their Outseta account
and use their credentials with your app to attain an Outseta token for an necessary API calls your app is making to
Outseta for a user.

### Setup & Dependencies

#### List of Dependencies
* material
* bootstrap css
* lodash-es

#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
